import { Component } from '@angular/core';

@Component({
  selector: 'app-body',
  templateUrl: './body.component.html',
})
export class BodyComponent {
  mostrar = false;
  frase = {
    mensaje: 'Un gran poder conlleva una gran responsabilidad',
    autor: 'Ben Parker'
  };

  personajes = [
    'Spiderman',
    'Venom',
    'Dr Octopus'
  ];
}
